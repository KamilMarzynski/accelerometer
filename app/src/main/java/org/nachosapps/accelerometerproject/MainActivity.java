package org.nachosapps.accelerometerproject;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    TextView xAxis, yAxis, zAxis;
    Button save, start, stop, init, listen;
    boolean flag;
    List<String> measurementsX = new ArrayList<>();
    List<String> measurementsY = new ArrayList<>();
    List<String> measurementsZ = new ArrayList<>();
    List<String> time = new ArrayList<>();
    TextView myLabel;
    EditText myTextbox;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    int counter;
    volatile boolean stopWorker;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private OutputStream outputStream;
    private InputStream inStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();


        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener((SensorEventListener) this, accelerometer,
                SensorManager.SENSOR_DELAY_FASTEST);


        save.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                writeToFile(measurementsX.toString(), "X.txt");
                writeToFile(measurementsY.toString(), "Y.txt");
                writeToFile(measurementsZ.toString(), "Z.txt");
                writeToFile(time.toString(), "T.txt");
            }
        });

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = true;
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = false;
            }
        });

        init.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init();
            }
        });

        listen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beginListenForData();
            }
        });
    }

    private void writeToFile(String data, String filename) {
        try {
            File file = new File(getApplicationContext().getExternalFilesDir(null),
                    filename);
            FileOutputStream fileOutput = new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutput);
            outputStreamWriter.write(data);
            outputStreamWriter.flush();
            fileOutput.getFD().sync();
            outputStreamWriter.close();
        } catch (Exception e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private void initViews() {
        xAxis = findViewById(R.id.axisX);
        yAxis = findViewById(R.id.axisY);
        zAxis = findViewById(R.id.axisZ);
        save = findViewById(R.id.btnSave);
        start = findViewById(R.id.btnStart);
        stop = findViewById(R.id.btnStop);
        init = findViewById(R.id.btnInit);
        listen = findViewById(R.id.btnNasluch);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor mySensor = event.sensor;


        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER && flag) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            String xTxt = "" + x;
            String yTxt = "" + y;
            String zTxt = "" + z;
            String timestamp = new Timestamp(System.nanoTime()).toString();

            measurementsX.add(xTxt);
            measurementsY.add(yTxt);
            measurementsZ.add(zTxt);
            time.add(timestamp);

            xAxis.setText(xTxt);
            yAxis.setText(yTxt);
            zAxis.setText(zTxt);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void init() {
        startActivity(new Intent(MainActivity.this, polaczenie.class));
        finish();
    }


//    @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.KITKAT)
//    public void run() {
//        final int BUFFER_SIZE = 1024;
//        byte[] buffer = new byte[BUFFER_SIZE];
//        int bytes = 0;
//        int b = BUFFER_SIZE;
//
//        while (true) {
//            try {
//                bytes = inStream.read(buffer, bytes, BUFFER_SIZE - bytes);
//                Log.e("bytes", bytes + "");
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    void beginListenForData() {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable() {
            public void run() {
                while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                    try {
                        int bytesAvailable = mmInputStream.available();
                        if (bytesAvailable > 0) {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);
                            for (int i = 0; i < bytesAvailable; i++) {
                                byte b = packetBytes[i];
                                if (b == delimiter) {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0,
                                            encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable() {
                                        public void run() {
                                            Log.e("Stringo msg", data);
                                            Toast.makeText(getApplicationContext(), data,
                                                    Toast.LENGTH_LONG).show();
                                        }
                                    });
                                } else {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    } catch (IOException ex) {
                        stopWorker = true;
                    }
                }
            }
        });

        workerThread.start();
    }

}
